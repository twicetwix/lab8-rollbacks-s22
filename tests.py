import os
import unittest

import psycopg2

from homework import buy_product


class TriggerConstraintTestCase(unittest.TestCase):

    def setUp(self) -> None:
        DB_NAME = os.environ.get("POSTGRESQL_DB")
        DB_HOST = os.environ.get("POSTGRESQL_HOST")
        DB_USER = os.environ.get("POSTGRESQL_USER")
        DB_PASS = os.environ.get("POSTGRESQL_PASS")

        self.conn = psycopg2.connect(f"dbname={DB_NAME} host={DB_HOST} password={DB_PASS} user={DB_USER}")
        with self.conn as conn:
            with conn.cursor() as cursor:
                cursor.execute("INSERT INTO player (username) VALUES "
                               "('test_skyline'),"
                               "('test_other_user')"
                               "ON CONFLICT DO NOTHING;")

                cursor.execute("INSERT INTO shop (product, in_stock, price) VALUES "
                               "('test_oreo_cookies', 400, 50),"
                               "('test_kitkat_chocolate', 200, 100)"
                               "ON CONFLICT DO NOTHING;")

                conn.commit()

    def tearDown(self) -> None:
        with self.conn as conn:
            with conn.cursor() as cursor:
                cursor.execute("DELETE FROM player WHERE username LIKE 'test_%'")
                cursor.execute("DELETE FROM shop WHERE product LIKE 'test_%'")
                conn.commit()

    def test_trigger_constraint_1_product(self):
        buy_product('test_other_user', 'test_oreo_cookies', 20)
        buy_product('test_other_user', 'test_oreo_cookies', 20)
        buy_product('test_other_user', 'test_oreo_cookies', 20)
        buy_product('test_other_user', 'test_oreo_cookies', 20)
        buy_product('test_other_user', 'test_oreo_cookies', 20)

        raised = False
        try:
            # expect trigger constraint violation
            buy_product('test_other_user', 'test_oreo_cookies', 10)
        except Exception as err:
            msg: str = err.args[0]
            if msg == "Too many items in inventory":
                raised = True
            else:
                raise err

        self.assertEqual(raised, True, "No trigger constraint exception was raised!")

    def test_trigger_constraint_2_products(self):
        buy_product('test_skyline', 'test_oreo_cookies', 50)
        buy_product('test_skyline', 'test_kitkat_chocolate', 50)

        raised = False
        try:
            # expect trigger constraint violation
            buy_product('test_skyline', 'test_oreo_cookies', 1)
        except Exception as err:
            msg: str = err.args[0]
            if msg == "Too many items in inventory":
                raised = True
            else:
                raise err

        self.assertEqual(raised, True, "No trigger constraint exception was raised!")

