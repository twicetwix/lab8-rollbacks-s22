


import os

import psycopg2

DB_NAME = os.environ.get("POSTGRESQL_DB")
DB_HOST = os.environ.get("POSTGRESQL_HOST")
DB_USER = os.environ.get("POSTGRESQL_USER")
DB_PASS = os.environ.get("POSTGRESQL_PASS")

conn = psycopg2.connect(f"dbname={DB_NAME} host={DB_HOST} password={DB_PASS} user={DB_USER}")

inventory_update = "INSERT INTO inventory (username, product, total) VALUES (%(username)s, %(product)s, %(amount)s) " \
                   "ON CONFLICT (username, product) DO UPDATE SET total = inventory.total + EXCLUDED.total;"
price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"


def buy_product(username, product, amount):
    with conn:
        with conn.cursor() as cur:
            obj = {"product": product, "username": username, "amount": amount}

            try:
                cur.execute(inventory_update, obj)
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Bad balance")
            except psycopg2.errors.RaiseException as e:
                raise Exception("Too many items in inventory")

            try:
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Product is out of stock")

            conn.commit()