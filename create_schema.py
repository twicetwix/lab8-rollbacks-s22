import os

import psycopg2

DB_NAME = os.environ.get("POSTGRESQL_DB")
DB_HOST = os.environ.get("POSTGRESQL_HOST")
DB_USER = os.environ.get("POSTGRESQL_USER")
DB_PASS = os.environ.get("POSTGRESQL_PASS")

conn = psycopg2.connect(f"dbname={DB_NAME} host={DB_HOST} password={DB_PASS} user={DB_USER}")

with conn:
    with conn.cursor() as cursor:
        cursor.execute("CREATE TABLE IF NOT EXISTS Player ("
                       "  username TEXT PRIMARY KEY,"
                       "  balance INT CHECK(balance >= 0)"
                       ");")
        cursor.execute("CREATE TABLE  IF NOT EXISTS Shop ("
                       "  product TEXT PRIMARY KEY , "
                       "  in_stock INT CHECK(in_stock >= 0),"
                       "  price INT CHECK (price >= 0)"
                       ");")
        cursor.execute("CREATE TABLE IF NOT EXISTS Inventory ("
                       "  id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,"
                       "  username TEXT NOT NULL, "
                       "  product TEXT NOT NULL, "
                       "  total INT CHECK (total > 0), "
                       "  UNIQUE (username, product),"
                       "  FOREIGN KEY (username) REFERENCES Player (username) ON DELETE CASCADE,"
                       "  FOREIGN KEY (product) REFERENCES Shop (product) ON DELETE CASCADE"
                       ");")
        cursor.execute("CREATE OR REPLACE FUNCTION ensure_inventory_total() RETURNS trigger LANGUAGE plpgsql AS "
                       "$$BEGIN "
                       "IF (SELECT SUM(total) FROM Inventory WHERE username = NEW.username) > 100"
                       " THEN RAISE EXCEPTION 'too many items in inventory'; "
                       "END IF; "
                       "RETURN OLD;"
                       "END;$$;")
        cursor.execute("CREATE CONSTRAINT TRIGGER total_inventory"
                       " AFTER INSERT OR UPDATE ON Inventory"
                       " NOT DEFERRABLE "
                       "FOR EACH ROW EXECUTE PROCEDURE ensure_inventory_total();")
        conn.commit()